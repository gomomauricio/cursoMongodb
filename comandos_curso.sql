
-- Borrar los datos de un esquema
-- db.productos.deleteMany({})

db.productos.find().pretty()

db.productos.insertOne({producto:"libro1",precio:9.99})

db.productos.insertOne({titulo:"Pelicula",vendedor:{nombre:"Juan",edad:25}})
db.productos.insertOne({nombre:"Camiseta",precio:19.99})

 db.empresa.insertOne({nombre:"Empresa A S.L.", esStartup:true, empleados:33, caja:1234578901234567890,detalles:{CEO:"Sebastian"},etiquetas:[{industria:"IT"},{industria2:"Aprendizaje"}],fecha: new Date(), insertado: new Timestamp()})

db.numero.insertOne({a:1})


use aficion
db.aficiones.insertMany([{nombre:"yoga"},{nombre:"Futbol"},{nombre:"Runnig"}])

/********** READ Metodos filtros y operadores *****************************/

------- EMPLEADOS
 mongoimport empleados.json -d dbempleados -c empleados --jsonArray --drop

use dbempleados
db.empleados.insertOne({  "EmpID" : 98,"Apellido" : "Sanchez","Nombre" : "Jorge", "Categoría" : "Administrador de Sistemas","Fecha contrato" : "13/6/05", "Oficina" : 1,"Extension" : "200", "Informes a" : 19 })

db.empleados.findOne() --devuelve el primer registro encontrado
db.empleados.find() --devuelve todos los registros

db.empleados.find( {"Salario anual":48500} ).count() --devuelve la cantidad 




 

------ $regex
db.empleados.find( {"Categoría":{$regex: /Jefe/}} ).pretty()  


------ $exists
db.empleados.find( {"Salario anual":{$exists:false}} ).pretty()  

------ $type  --busca un campo si es de un tipo especifico
db.empleados.find( {"Salario anual":{$type:"number"}} ).pretty()  
db.empleados.find( {"Salario anual":{$type:"number",$gt:44000}} ).pretty()  


/******* LOGICOS ******/

-----NOT $not
db.empleados.find( {"Salario anual":{$eq:48500}} ).count()
db.empleados.find( {"Salario anual":{ $not: {$eq:48500} }} ).count()
db.empleados.find( {"Salario anual":{$ne:48500}} ).count()

-----NOR $nor
db.empleados.find({$nor:[{"Salario anual":48000},{"Oficina":4}]}).pretty()

-----NOR $or
db.empleados.find({$nor:[{"Salario anual":48000},{"Oficina":4}]}).pretty()

-----AND $and
db.empleados.find({$and:[{"Salario anual":48500},{"Oficina":4}]}).pretty()

-----OR $or
db.empleados.find({$or:[{"Salario anual":48000},{"Oficina":4}]}).pretty()

-- compara con una lista si esta el elemento : $in ||  compara con una lista si no esta el elemento : $nin 
db.empleados.find({"Salario anual":{$nin:[49000,49000]}}).pretty()

-- menor que: $lt  || menor igual que: $lte
db.empleados.find({"Salario anual":{$lt:49000}}).pretty()

-- mayor que: $gt  || mayor igual que: $gte
db.empleados.find({"Salario anual":{$gt:49000}}).pretty()

-- no igual a: $ne
db.empleados.find({"Salario anual":{$ne:49000}}).pretty()

db.empleados.find({Apellido:"Barreto"}).pretty()
db.empleados.find({"Salario anual":49000}).pretty()
db.empleados.findOne({"Salario anual":49000})
db.empleados.find({"Salario anual":{$eq:49000}}).pretty()





/****** Importar desde archivo *******/

-- mongoimport archivo.jason -d nombreBaseDatos -c nombreColeccion --especifica que es un arreglo y el drop para borra si es que existe 

mongoimport colores.json -d dbcolores -c colores --jsonArray --drop


/************ RELACION n : n  *********/
 
------- libros -----
use libros
db.libro.insertOne({nombre:"Un libro", autores:[{nombre:"Juan",edad:25},{nombre:"Andrea",edad:25}]})
db.libro.updateOne({},{$set:{autores:[ObjectId("632cf6f8d481e9156bf8538c"),ObjectId("632cf6f8d481e9156bf8538d")]}})
db.libro.find().pretty()
db.autores.insertMany([{nombre:"Juan",edad:25,direccion:"Calle A 35"},{nombre:"Andrea",edad:22,direccion:"Calle B 12"}])
db.autores.find().pretty()

------- tienda -----
use tienda
db.productos.insertOne({titulo:"Un libro",precio:9.99})
db.productos.find().pretty()
db.clientes.insertOne({nombre:"Juan",edad:25})
db.clientes.updateOne({},{$set:{pedidos:[{idProducto:ObjectId("632cf2c4d481e9156bf85387"),cantidad:3}]}})
db.clientes.find().pretty()
db.pedidos.insertOne({idproducto:ObjectId("632cf2c4d481e9156bf85387"),idCliente:ObjectId("632cf2fed481e9156bf85388")})
db.pedidos.find().pretty()



/************ RELACION 1 : n  *********/
 
------- ciudades -----
use ciudades
db.ciudad.insertOne({nombre:"Barcelona",poblacion:5575})
db.ciudad.find().pretty()
db.ciudadanos.insertMany([{nombre:"Juan",ciudad: ObjectId("632cf117d481e9156bf85382")},{nombre:"Sebastian",ciudad: ObjectId("632cf117d481e9156bf85382")}])
db.ciudadanos.find().pretty()
db.ciudadanos.deleteMany({})

------- Preguntas y respuestas -----
use preguntsyrespuestas
db.pregunta.insertOne({nombre:"Juan",pregunta:"Como funciona esto?", respuestas:["p1r1","p1r2"]})
db.pregunta.find().pretty()
db.respuesta.insertMany([{_id:"p1r1",texto:"Funciona asi . . . "},{_id:"p1r2",texto:"Gracias!!!"}])
db.respuesta.find().pretty()
db.pregunta.deleteMany({})
db.pregunta.insertOne({nombre:"Juan",pregunta:"Como funciona esto?", respuestas:[{texto:"Funciona asi . . . "},{texto:"Gracias!!!"}]})





/************ RELACION 1 : 1  *********/
------- COCHES -----
use coches
db.personas.insertOne({nombre:"Juan",edad:25,sueldo:2000,coche:{modelo:"Toyota",precio:30000}})
db.personas.find().pretty()
db.personas.insertOne({nombre:"Juan",edad:25,sueldo:2000,coche:"coche1"})
db.coche.insertOne({_id:"coche1",modelo:"Toyota",precio:30000})
db.coche.find().pretty()


-------- Hospital ------



use hospital
db.pacientes.insertOne({nombre:"Juan",edad:25,historial:"historial1"})
db.pacientes.find().pretty()
db.historial.insertOne({_id:"historial1", enfermedades:["brazo roto", "gripe"]})
db.historial.find().pretty()
db.pacientes.find({nombre:"Juan"})
db.pacientes.findOne({nombre:"Juan"}).historial
var his = db.pacientes.findOne({nombre:"Juan"}).historial
db.historial.findOne({_id:his}).enfermedades

db.pacientes.deleteMany({})
db.pacientes.insertOne({nombre:"Juan",edad:25,historial:{enfermedades:["brazo roto","gripe"]}})
db.pacientes.findOne({nombre:"Juan"}).historial.enfermedades



