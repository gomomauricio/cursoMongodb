
/************** DELETE *****************/
db.usuarios.deleteOne({nombre:"Mario"})
db.usuarios.deleteMany({nombre:{$in:["Mario","Maria"]}})
db.usuarios.deleteMany({}) --borramos los documentos de la coleccion
db.usuarios.drop()  -- borra toda la coleccion incluyendo documentos
db.dropDatabase() -- borra la base de datos con todas sus colecciones


/***************************  UPDATE   **************************/
db.usuarios.updateMany({nombre:{$in:[ "Juan", "Alfonso"]}},{$set: {notas:[{año:1,mates:0,ingles:0}]}})

---- $unset       --quita los campos que queramos
db.usuarios.updateMany({nombre:{$in:[ "Juan", "Alfonso"]}},{$unset: {notas:[{año:1,mates:0,ingles:0}]}})

--- $rename
db.usuarios.updateOne({nombre:"Juan"},{$rename:{nombre:"Nombre1"}})


--- $inc
db.usuarios.updateOne({nombre:"Juan"}, {$inc:{edad:1}})
db.usuarios.find({nombre:"Juan"}).pretty()

--- $min
db.usuarios.updateOne({nombre:"Juan"}, {$min:{edad:20}})  --solo actualiza si es menor que el valor actual

--- $max
db.usuarios.updateOne({nombre:"Juan"}, {$max:{edad:40}})  --solo actualiza si es mayor que el valor actual

--- $mul
db.usuarios.updateOne({nombre:"Juan"}, {$mul:{edad:1.1}})  --nos incrementa la edad 10%

use dbempleados 
db.empleados.find()

-----  sort
db.empleados.find().sort({"EmpID":1}).pretty()
db.empleados.find().sort({"EmpID":-1}).pretty()
db.empleados.find().sort({"Oficina":1,"EmpID":1}).pretty()


------ $skip  |   $limit
db.empleados.find().sort({"EmpID":1}).skip(10).pretty()
db.empleados.find().sort({"EmpID":1}).skip(10).limit(2).pretty()
db.empleados.find().limit(2).pretty()
-----  cursor
const objetoCursor = db.empleados.find()
objetoCursor.next()  --muestra la lista de uno por uno

-- proyeccion
db.empleados.find({},{Nombre:1}).pretty()
db.empleados.find({},{Nombre:1,"Salario anual":1}).pretty()
db.empleados.find({},{Nombre:1,"Salario anual":1,_id:0}).pretty() --quito el id

/********   USUARIOS ************/
mongoimport usuarios.json -d dbusuarios -c usuarios --jsonArray --drop
use dbusuarios
db.usuarios.find().pretty()

db.usuarios.find({aficiones:"correr"},{"aficiones":1}).pretty()
db.usuarios.find({aficiones:"correr"},{"aficiones.$":1}).pretty()   --al agregar .$ solo muestra lo que escribimos(correr)

db.usuarios.find({nombre:"David"}).pretty()  

--- $slice
db.usuarios.find({nombre:"David"},{"aficiones":{$slice:1}}).pretty()  
db.usuarios.find({nombre:"David"},{"aficiones":{$slice:2}}).pretty()  


db.usuarios.find({ dirección:{ ciudad: "Barcelona"} }  ).pretty()     --hace una busqueda EXACTA
db.usuarios.insertOne({nombre:"David", dirección:{ciudad:"Barcelona", calle:"Avenida Madrid"}})
db.usuarios.insertOne({nombre:"Alfonso", dirección:{ calle:"Avenida Madrid", ciudad:"Barcelona"}})
db.usuarios.find({ "dirección.ciudad": "Barcelona"}).pretty()     --hace una busqueda mas detallada

db.usuarios.updateOne({nombre:"David"},{$set: {aficiones:["correr","nadar","patinar"]}})
db.usuarios.updateOne({nombre:"Alfonso"},{$set: {aficiones:["correr","nadar"]}})
db.usuarios.updateOne({nombre:"Felipe"},{$set: {aficiones:["nadar","correr"]}})
db.usuarios.updateOne({nombre:"Felipe"},{$set: {notas:[{año:1,mates:90,ingles:95},{año:1,mates:83,ingles:82}]}})
db.usuarios.updateOne({nombre:"David"},{$set: {notas:[{año:1,mates:80,ingles:85},{año:1,mates:86,ingles:79}]}})


db.usuarios.find({$and: [{"notas.mates": {$gt:85}},{"notas.ingles":{$gt:80}}]}).pretty() 
db.usuarios.find({notas: {$elemMatch: {mates: {$gt:85} , ingles:{$gt:80}}}}).pretty() 

------ $size   
db.usuarios.find({aficiones:{$size:2}}).pretty() 

------ $all
db.usuarios.find({aficiones:{$all:["nadar","correr"]}}).pretty() 


use datosfinancieros
db.ventas.insertMany([{ventas:50,objetivo:7},{ventas:90,objetivo:90},{ventas:300,objetivo:150}])
db.ventas.find().pretty()

------ $expr   
db.ventas.find({$expr: { $gt:["$ventas","$objetivo"]}}).pretty() --busqueda donde ventas es mayor que objetivo


------ $expr    condicional
db.ventas.find({$expr: {$gt: [{$cond: {if: {$lt:["$ventas",100]}, then: {$subtract: ["$ventas",20]}, else:"$ventas" }},"$objetivo"]}}).pretty() --busqueda donde ventas es mayor que objetivo mayores a 20

