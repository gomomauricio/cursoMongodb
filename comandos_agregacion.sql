

/******** PERSONAS *****/

mongoimport personas.json -d dbpersonas -c personas --jsonArray --drop

use dbpersonas

db.personas.findOne()

db.personas.aggregate([{$match:{gender:"female"}}]).pretty()


db.personas.aggregate([
    {$match:{gender:"female"}},
    {$group: {_id:{ciudad:"$location.city"}, personas:{$sum:2}}}
]).pretty()




db.personas.aggregate([
    {$match:{gender:"female"}},
    {$group: {_id:{ciudad:"$location.city"}, personas:{$sum:2}}},
    {$sort: {personas: -1}}
]).pretty()



db.personas.aggregate([
    {$project:{_id:0,gender:1,NombreCompleto:{$concat:["$name.first"," ", "$name.last"]}}}
]).pretty()

db.personas.aggregate([
    {$project:
        {
            _id:0,
            gender:1,
            NombreCompleto:
                {
                    $concat:
                        [
                           { $toUpper:"$name.first" },
                                         " ", 
                            { $toUpper: "$name.last" }
                        ]
                }
        }
    }
]).pretty()




db.personas.aggregate([
    {$project:
        {
            _id:0,
            gender:1,
            NombreCompleto:
                {
                    $concat:
                        [
                           { $toUpper: {$substrCP:["$name.first",0,1]} },
                           { $substrCP:["$name.first",1,{$subtract:[{$strLenCP:"$name.first"},1]}]},
                                         " ", 
                           { $toUpper: {$substrCP:["$name.last",0,1]} },
                           { $substrCP:["$name.last",1,{$subtract:[{$strLenCP:"$name.last"},1]}]},
                        ]
                }
        }
    }
]).pretty()







