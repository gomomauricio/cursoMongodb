-------- Restaurantes ------
 
 
use restaurantes

-----restaurante
db.restaurante.insertOne({_id:"r1",nombre:"KFC",descripcion:"Los mejores pollos rostizados", direccion:"Calle Ladud #23",imagen:"https://cdn.recetas360.com/wp-content/uploads/2020/03/como-hacer-el-pollo-kfc.jpeg"})
db.restaurante.insertOne({_id:"r2",nombre:"Burger King",descripcion:"Todo tipo de hamburgesas", direccion:"Calle Puig 4",imagen:"https://financierolatam.com/wp-content/uploads/2020/07/PorfiriosPor.jpg"})





-----turno
db.turno.insertOne({_id:"turno1",turno:"TURNO_17_00"})
db.turno.insertOne({_id:"turno2",turno:"TURNO_18_00"})
db.turno.insertOne({_id:"turno3",turno:"TURNO_19_00"})
db.turno.insertOne({_id:"turno4",turno:"TURNO_20_00"})





-----mesa
db.mesa.insertOne({_id:"mesa1",mesa:"Mesa 1"}) 
db.mesa.insertOne({_id:"mesa2",mesa:"Mesa 2"}) 
db.mesa.insertOne({_id:"mesa3",mesa:"Mesa 3"}) 
db.mesa.insertOne({_id:"mesa4",mesa:"Mesa 4"}) 
db.mesa.insertOne({_id:"mesa5",mesa:"Mesa 5"}) 





-----reserva 
db.reserva.insertOne({restaurante:"r1",personas:3,fecha:"2022-09-23",turno:"turno2"})

 

 
db.pacientes.find().pretty()
 
 
db.pacientes.find({nombre:"Juan"})
db.pacientes.findOne({nombre:"Juan"}).historial
var his = db.pacientes.findOne({nombre:"Juan"}).historial
db.historial.findOne({_id:his}).enfermedades

db.pacientes.deleteMany({})
db.pacientes.insertOne({nombre:"Juan",edad:25,historial:{enfermedades:["brazo roto","gripe"]}})
db.pacientes.findOne({nombre:"Juan"}).historial.enfermedades



/*

Reserva
[
    id,
    restaurante,
    personas,
    fecha,
    turno
    { idMongo, r1, 3, 2022-09-21, turno1 }
]

Restaurante
[
    id,
    nombre,
    descripcion,
    direccion,
    imagen(URL)
    { r1, kfc, venta de pollo, calle galindo #5, https://restaurantdemexico.com.mx/ }
    
]
 
Turno
[
    id,
    turno
    { turno1, TURNO_10_00 }
]

Mesas
[
    id,
    capacidad,
    mesa( numeradas del 1 al 10,2 etc.)
    { mesa1, mesa 1 }

]
*/