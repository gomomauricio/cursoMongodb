--  
* NoSQL = Not Only SQL (sin estructura fija)

* Formato JSON(binario BSON, "Clave":"Valor") JAvaScript Object Notation

* Ventajas [ mas rapido, no hace falta unir tablas, mas flexible]

* CMD general : mongo | detener : net stop MongoDB (modo Admin)
* *   Iniciar : mongod --dbpath "D:\Programas\MongoDB\data"

* Drivers : https://www.mongodb.com/docs/drivers/

* Ctrl + l  para vaciar la pantalla de la terminal 

* Se pueden tener hasta 100 documentos anidados uno: {dos: {tres: {etc:... }}} 
* Un documento puede tener hasta 16megas de contenido
 
* [Tipo Datos](https://www.mongodb.com/docs/manual/reference/bson-types/) 
* [Operadores](https://www.mongodb.com/docs/manual/reference/operator/query/) 
* [Cursor](https://www.mongodb.com/docs/manual/tutorial/iterate-a-cursor/)
* [Update](https://www.mongodb.com/docs/manual/reference/operator/update/)
* [Aggregation](https://www.mongodb.com/docs/manual/reference/operator/aggregation-pipeline/)

* Tipo Datos
    * * Texto: "Juan"
    * * Boolean: true/false
    * * Numero: NumberInt(32) | NumberLong(64) | NumberDecimal 100.23
    * * ObjectID ObjectId --> identificador de registro
    * * ISODate ISODate("2020-10-21")
    * * Timestamp Timestamp(1412180887, 1)
    * * Documento Incrustado { "documento" : { . . . }}
    * * Array (Matriz) { "matriz": [...]}

