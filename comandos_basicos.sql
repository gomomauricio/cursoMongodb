show dbs --muestra las bases de datos existentes
show collections --muestra los documentos

ctrl + l -- en la terminal limpia la pantalla

use tienda --crea una nueva base de datos

--switched to db productos --se cambia a esa base

db.productos.insertOne({producto:"libro1",precio:9.99}) --crea una coleccion nueva, insertando los datos

db.productos.find() --muestra todos los documentos de la coleccion

db.productos.find().pretty() --muestra todos los documentos de la coleccion con un salto de linea

db.nombres.find().pretty().toArray() --para que regrese todos los documentos

 db.nombres.find().pretty().forEach( (nombres) => {printjson(nombres)} )

--se puede insertar otro campo a la coleccion, incluso otra coleccion
db.productos.insertOne({producto:"libro2",precio:12.99,descripcion:"Libro de ciencia ficcion", detalles:{tapa:"dura",paginas:300}})

--DELETE
db.usuarioDatos.deleteOne({nombre:"Juan"})
db.usuarioDatos.deleteMany({alta : true})



--UPDATE
db.usuarioDatos.updateOne({nombre:"Juan"},{$set: {edad:28}})
db.nombres.updateMany({},{$set:{estado:{descrpcion:"activo", actualizado:"Hace 1 hora"}}})


-- SELECT
db.usuarioDatos.find({nombre:"Juan"}).pretty()
db.usuarioDatos.find({edad:{$gt:24}}).pretty()
db.nombres.findOne({nombre:"Felipe Gonzalez"}).aficiones
db.nombres.findOne({"estado.descrpcion":"activo"})


-- Proyeccion
db.nombres.find({},{nombre:1, _id:0})

--Matriz
db.nombres.updateOne({nombre:"Felipe Gonzalez"}, {$set: {aficiones:["cantar","bailar","presentar"]} })

-- BORRAR UN DOCUMENTO DE LA BASE
db.usuarioDatos.drop()

-- BORRAR TODA LA BASE DE DATOS
db.dropDatabase()


-- Para ver el tipo de dato
typeof db.numbers.findOne().a