# Código del curso MongoDB 
# Instructor
> [Sebastian Tunnell](https://www.udemy.com/course/mongodb-es/) 
 

# Udemy
* MongoDB - NoSQL, Operaciones CRUD, Schemas de datos, Relaciones y Framework de Agregación

## Contiene

* Bases de datos NoSQL con MongoDB
* Operaciones CRUD en MongoDB
* Diseñar bases de datos con schemas y relaciones
* Framework de agregación para transformar datos
 
 
---

## Notas
 
~~~
* Repositorio solo en ** Gitlab  **

  

---
## Código 

`//desarrollo` 
 `public int suma( int x, int y ) { return x + y; }` 

 `//test` 
`@Test`
`public int suma( int x, int y ) {  assertEquals(2, ( x + y ) ); }`


 

---
#### Version  V.0.1 
* **V.1.0**  _>>>_  Iniciando proyecto [  on 12 AGO, 2022 ]  
 
 